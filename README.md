# Numisma
Um eine verschollene antike Münze (lat.: "numisma") wiederzufinden, müssen die Bewohner eines Studentenwohnheims im 19. Jahrhundert identifiziert werden, indem man ihre Zimmer durchsucht und die Hinweise richtig verknüpft.

**HINWEISE UND TIPPS FÜRS SPIEL GANZ UNTEN**

<img src="./Assets/Sprites/Backgrounds/Room1Background.png" alt="Image of Room 1" width="600"/>

## Geschichte
Wir schreiben das Jahr 1884. Du heißt Boy Matthiessen und bist Philosophie-Student an der Christian-Albrechts-Universität zu Kiel.\
Eines Morgens erwachst du nach einer langen Partynacht im Studentenwohnheim und musst erschrocken feststellen, dass etwas in deinem Zimmer fehlt: die antiken Wikingermünzen, die dein Vater, ein alter Schiffskapitän, dir im Vertrauen überlassen hat.

Hat einer deiner Partygäste sie gestohlen? Aber falls ja - wie willst du herausfinden, wer es war und wo er sie versteckt hat?\
Es waren so viele Studenten so unterschiedlicher Jahrgänge auf deiner Party...

Zum Glück bist du im Besitz eines Buches mit den Matrikeldaten sämtlicher Studenten der Christian-Albrechts-Universität von 1855 an.\
Und möglicherweise hat ja einer der Gäste Spuren in deinem Zimmer hinterlassen, die auf seine Identität schließen lassen...

Kombiniere die Hinweise und löse damit die Rätsel, um dich durch das Studentenwohnheim zu kämpfen und dem Übeltäter auf die Spur zu kommen!\
Vielleicht schaffst du es, dir die Münzen zurückzuholen, bevor dein Vater etwas von deren Verschwinden mitbekommt...



Wir schreiben nun das Jahr 1905.\
Auf Sylt wurden 132 Silbermünzen aus der Wikingerzeit gefunden. Dr. Ralf Wiechmann hat den Schatzfund im Rahmen seiner Promotion untersucht und bestimmt.

Doch wie sind die Münzen überhaupt nach Westerland gekommen? Und was hat der Schatzfund mit den Geschehnissen im Studentenwohnheim von 1884 zu tun?

Zu dieser Frage haben wir unsere ganz eigene Theorie aufgestellt, die in NUMISMA erlebbar wird.\
Schlüpfe in die Rolle eines Studenten zum Ende des 19. Jahrhunderts, löse kniffelige Rätsel und erfahre, wie es sich historisch höchstwahrscheinlich nie zugetragen hat.


## Daten
- [Matrikeldaten der CAU](https://codingdavinci.de/daten/matrikeldaten-der-cau)
- [Gattungslehre und Abbildungen von Insekten von der Universitätsbibliothek Kiel](https://codingdavinci.de/daten/gattungslehre-und-abbildungen-von-insekten)
- [Der Schatzfund von Westerland](https://codingdavinci.de/daten/der-schatzfund-von-westerland)
- [Stormarnsche Zeitung](https://codingdavinci.de/daten/stormarnsche-zeitung)

## Spielen

### Windows
Um unter Windows zu spielen, navigiere [in dieses](./Builds/Windows) Verzeichnis und klicke oben rechts auf das Download Icon. Klicke dort unter "Download this directory" auf die Option für zip und lade den Ordner im sich öffnenden Download-Dialog herunter. Entpacke den Ordner auf deinem Computer und starte das darinliegende "Numisma.exe".

### Linux
Wenn du unter Linux spielen möchtest, navigiere [in dieses](./Builds/Linux) Verzeichnis und klick oben rechts auf das Download Icon. Klicke dort unter "Download this directory" auf deine bevorzugte Kompressionsvariante. "tar.gz" funktioniert normalerweise problemlos. Entpacke den Ordner auf deinem Computer und starte das Spiel durch Ausführen der Datei "Numisma.x86_64".

### Mac
Die Mac Version ist unter [diesem Verzeichnis](./Builds/MacOS/MacOS.app) zu finden. Auch dort klicke oben rechts auf das Download-Symbol und klicke dann unter "Download this directory" auf "zip", um den Download als zip zu starten. Entpacke den Ordner auf deinem Computer und starte die App. Eventuell musst du vorher in den Systemeinstellungen einstellen, dass du die App starten kannst, da sie von einem "nicht verifizierten Entwickler" stammt.

### Browser
Das Spiel ist im Browser unter der [Gitlab-Page](https://liktoria.gitlab.io/numisma) verfügbar. Der Vollbild-Modus funktioniert leider momentan nur bei 16:9 Bildschirmen zuverlässig. Für schönere Übergänge bei der Musik empfiehlt es sich, eine der anderen Versionen herunterzuladen und lokal zu spielen. Ebenfalls ist es möglich, das Repository zu klonen und das Spiel über Unity als Anwendung bauen zu lassen.

## Das Team

### Programmierung
- Annika Neumann (Liktoria)
- Jakob Vogt
- Fabian Müller

### Rätsel, Geschichte, Design & Zeichnungen
- Annabelle Junger
- Lara Krisch
- Johanna Simon
- Wiebke Brommundt

## Sounds und Musik
Alle Sounds und Musik wurden erstellt, komponiert und implementiert von Ali Popa.

## Weiteres Material
Neben der fleißigen Arbeit aller Gruppenmitglieder wurde zusätzlich folgendes Material genutzt:
- [Schriftart Prodelt Co](https://www.dafont.com/de/prodelt-co.font)
- [Schriftart Quentin](https://www.dafont.com/de/quentin-2.font?)
- [Schriftart Floane](https://www.dafont.com/de/floane.font?l[]=1)
- [Unity Blur Shader](https://void1gaming.itch.io/unity-blur-shader)

## Lösungshinweise zu den einzelnen Räumen

<details>
  <summary>Lösungshinweise</summary>
  
<details>
  <summary>Raum 1</summary>

  <details>
  <summary>Mantel</summary>
  Hat etwa jemand seinen Mantel vergessen? Vielleicht findet sich ein Hinweis auf den Namen des Eigentümers. 
  </details>
    
  <details>
  <summary>Geburtstag</summary>
  Schaue dir den Mantel genauer an. Gibt es noch mehr Hinweise, wer der Eigentümer sein könnte?
  </details>
    
  <details>
  <summary>Versteck</summary>
  Wo könnten meine Münzen versteckt sein? Im Schrank? Auf dem Boden? Unterm Bett?
  </details>

</details>
    
<details>
  <summary>Raum 2</summary>

  <details>
  <summary>Notizbuch</summary>
  Versuche den Text sichtbar zu machen. Findest du im Zimmer ein Objekt, welches dir dabei helfen könnte?
  (Objekte können auch über den Bildschirm gezogen werden.)
  </details>
    
  <details>
  <summary>Zahlenschloss der Truhe</summary>

  <details>
  <summary>Tipp 1</summary>
  Gibt es im Raum ein Rätsel, das dir fünf Zahlen liefert?
  </details>

  <details>
  <summary>Tipp 2</summary>
  Schau dir das Insektenbuch genauer an.
  </details>
  </details>

  <details>
  <summary>Insektenbuch</summary>
 
  <details>
  <summary>Tipp 1</summary>
  Lies die Notizzettel und schaue dir die Insektenbilder genau an, es sind Zahlen gesucht.
  </details>

  <details>
  <summary>Tipp 2 - Seite 5</summary>
  Es wird eine Zahl gesucht, zähle Segmente eines bestimmten Körperteils.
  </details>
  </details>

  <details>
  <summary>Zeitung</summary>

  <details>
  <summary>Tipp 1</summary>
  Puzzle das Extrablatt korrekt zusammen. Was fällt dir auf?
  </details>

  <details>
  <summary>Tipp 2</summary>
  Beachte die verschiedenen Symbole unter den Buchstaben und den Hinweis unten rechts.
  </details>
  </details>

</details>
    
<details>
  <summary>Raum 3</summary>

  <details>
  <summary>Gifträtsel</summary>

  <details>
  <summary>Tipp 1</summary>
  Finde zunächst durch Regel 3 heraus, welche die ungiftige Flüssigkeit ist.
  </details>

  <details>
  <summary>Tipp 2</summary>
  Nutze dann aus den Regeln 1 und 4 die Informationen darüber, welche Flüssigkeiten aufeinanderfolgen müssen.
  </details>

  <details>
  <summary>Tipp 3</summary>
  Nun gibt es in Regel 2 nur noch einen möglichen Fall.
  </details>
  </details>
    
  <details>
  <summary>Zahlenschloss der Truhe</summary>
  
  <details>
  <summary>Tipp 1</summary>
  Gibt es im Raum ein Rätsel, das dir fünf Zahlen liefert?
  </details>

  <details>
  <summary>Tipp 2</summary>
  Schau dir das Pergament unter dem Widderkopf genauer an!
  </details>
  </details>
    
  <details>
  <summary>Spiegelrätsel</summary>
  
  <details>
  <summary>Tipp 1</summary>
  In der Truhe befindet sich etwas, das dir helfen kann, das Spiegelrätsel zu lösen.
  </details>

  <details>
  <summary>Tipp 2</summary>
      
  <details>
  <summary>Nur zu lesen, falls die Truhe bereits geöffnet wurde!</summary>
         
  <details>
  <summary>Tipp 2.1</summary>
  Welche Spiegelbildvariationen entsprechen den Längen, welche den Kürzen?
  </details>

  <details>
  <summary>Tipp 2.2</summary>
  Vergleiche die geometrischen Formen der Spiegelbilder mit denen der Morsesymbole.
  </details>

  <details>
  <summary>Tipp 2.3</summary>
  Jedes der vier Spiegelbilder steht für genau eine Wort.
  </details>
  </details>
  </details>
  </details>
  
  <details>
  <summary>Lyrikrätsel</summary>
  Tipps zum Lyrikrätsel tauchen aufgrund dessen Schwierigkeitsgrades direkt im Spiel auf.
  </details>  

  <details>
  <summary>Cryptex-Buchstabenrätsel</summary>
  
  <details>
  <summary>Tipp 1</summary>
  Es gibt acht Rädchen, die man nach unten drehen kann. Wie oft musst du die einzelnen Rädchen nach unten drehen?
  </details>

  <details>
  <summary>Tipp 2</summary>
  Im Raum muss es ein Rätsel geben, dessen Lösung dir diese acht Zahlen liefert.
  </details>
  </details>
  </details>
      
</details>

## Lizenz der Mediendateien
Shield: [![CC BY-SA 4.0][cc-by-sa-shield]][cc-by-sa]

Die Mediendateien dieser Arbeit sind lizensiert unter:
[Creative Commons Attribution-ShareAlike 4.0 International License][cc-by-sa].

[![CC BY-SA 4.0][cc-by-sa-image]][cc-by-sa]

[cc-by-sa]: http://creativecommons.org/licenses/by-sa/4.0/
[cc-by-sa-image]: https://licensebuttons.net/l/by-sa/4.0/88x31.png
[cc-by-sa-shield]: https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg
