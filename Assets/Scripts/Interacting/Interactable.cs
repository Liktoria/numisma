using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    protected InteractionManager interactionManager;
    protected Transform cameraTransform;
    protected Vector3 cameraPosition;
    protected SceneLoadManager sceneLoadManager;
    protected DialogueManager dialogueManager;

    void Start()
    {
        interactionManager = InteractionManager.GetInstance();
        sceneLoadManager = SceneLoadManager.GetInstance();
        dialogueManager = DialogueManager.GetInstance();
        cameraTransform = GameObject.Find("Main Camera").transform;
        InitializeSpecificAttributes();
    }

    protected virtual void InitializeSpecificAttributes()
    {

    }

    public virtual void ReceiveDialogueFinished()
    {
        
    }

    protected IEnumerator WaitForFade()
    {
        yield return new WaitForSeconds(1.0f);
        DoAfterFade();
    }

    protected virtual void DoAfterFade()
    {

    }
}
