using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : Interactable
{
    [SerializeField]
    protected string sceneToEnter;

    void OnMouseDown()
    {
        UpdateCurrentRoom();
        sceneLoadManager.GoToScene(sceneToEnter);
    }

    public void UpdateCurrentRoom()
    {
        if (sceneToEnter == "Hallway")
        {
            sceneLoadManager.gameState.currentRoom = 0;
        }
        else if (sceneToEnter == "Room1")
        {
            sceneLoadManager.gameState.currentRoom = 1;
        }
        else if (sceneToEnter == "Room2")
        {
            sceneLoadManager.gameState.currentRoom = 2;
        }
        else if (sceneToEnter == "Room3")
        {
            sceneLoadManager.gameState.currentRoom = 3;
        }
        else if(sceneToEnter == "Storeroom")
        {
            sceneLoadManager.gameState.currentRoom = 4;
        }
    }
}
