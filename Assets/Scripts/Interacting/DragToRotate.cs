using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragToRotate : MonoBehaviour
{
    [SerializeField]
    private float rotationSpeed = 20f;

    private float rotationX = 0;
    private float rotationY = 0;

    void OnMouseDrag()
    {
        rotationX = Input.GetAxis("Mouse X") * rotationSpeed * Mathf.Deg2Rad;
        rotationY = Input.GetAxis("Mouse Y") * rotationSpeed * Mathf.Deg2Rad;

        transform.RotateAround(Vector3.up, -rotationX);
        transform.RotateAround(Vector3.right, rotationY);
    }
}
