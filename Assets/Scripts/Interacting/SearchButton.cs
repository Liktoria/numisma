using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchButton : MonoBehaviour
{
    private InteractionManager interactionManager;

    // Start is called before the first frame update
    void Start()
    {
        interactionManager = InteractionManager.GetInstance();
    }

    public void SearchObject()
    {
            Inspectable currentInspectable = interactionManager.GetCurrentInspectable();
            currentInspectable.Search();
    }
}
