using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionManager : MonoBehaviour
{
    [SerializeField]
    [Tooltip("Drag the secondary camera in here.")]
    protected Camera secondaryCamera;

    [SerializeField]
    [Tooltip("Drag the inspect menu panel in here.")]
    public GameObject inspectMenuPanel;

    private bool inspectingSomething = false;
    private static InteractionManager instance;
    private GameObject[] interactables;
    private Inspectable currentInspectable = null;
    private bool firstBackChest;

    public static InteractionManager GetInstance()
    {
        return instance;
    }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public void BackToSceneView()
    {
        if (currentInspectable != null)
        {
            if (currentInspectable is Chest)
            {
                Chest chest = (Chest)currentInspectable;

                if(!SceneLoadManager.GetInstance().savedRoom2State.chestOpened && chest.chestInRoom2
                && currentInspectable.instantiatedModel.GetComponent<ShowLockPrefab>().lockIsShowing)
                {
                    currentInspectable.instantiatedModel.GetComponent<ShowLockPrefab>().HideLock();
                }
                else if(!SceneLoadManager.GetInstance().savedRoom3State.chestOpened && !chest.chestInRoom2
                && currentInspectable.instantiatedModel.GetComponent<ShowLockPrefab>().lockIsShowing)
                {
                    currentInspectable.instantiatedModel.GetComponent<ShowLockPrefab>().HideLock();
                }
                else if (SceneLoadManager.GetInstance().savedRoom2State.chestOpened && chest.chestInRoom2
                && currentInspectable.instantiatedModel.TryGetComponent(out ShowNewspaper newspaper)
                && newspaper.paperIsShowing)
                {
                    currentInspectable.instantiatedModel.GetComponent<ShowNewspaper>().HideNewspaper();
                }
                else if (SceneLoadManager.GetInstance().savedRoom3State.chestOpened && !chest.chestInRoom2
                && currentInspectable.instantiatedModel.TryGetComponent(out MorseAlphabet morseAlphabet)
                && morseAlphabet.morseIsShowing)
                {
                    currentInspectable.instantiatedModel.GetComponent<MorseAlphabet>().HideMorseAlphabet();
                }
                else
                {
                    currentInspectable.RemoveInstanitatedModel();
                    currentInspectable.currentlyInspecting = false;
                    currentInspectable = null;
                    inspectMenuPanel.SetActive(false);
                    BlurBackground(false);
                }
            }
            else if(currentInspectable is Stairs)
            {
                Stairs stairs = (Stairs)currentInspectable;
                stairs.StartLastDialogue();
                currentInspectable.RemoveInstanitatedModel();
                currentInspectable.currentlyInspecting = false;
                currentInspectable = null;
                inspectMenuPanel.SetActive(false);
                BlurBackground(false);                
            }
            else if(currentInspectable is TextInspectable && SceneLoadManager.GetInstance().gameState.textIsShowing)
            {
                TextInspectable currentText = (TextInspectable)currentInspectable;
                currentText.HideText();
            }
            else
            {
                currentInspectable.RemoveInstanitatedModel();
                currentInspectable.currentlyInspecting = false;
                currentInspectable = null;
                inspectMenuPanel.SetActive(false);
                BlurBackground(false);
            }
            
        }
           
    }

    public void SetInteractableCollisions(bool isClickable)
    {
        interactables = GameObject.FindGameObjectsWithTag("Interactable");

        foreach (GameObject interactable in interactables)
        {
            Collider2D[] colliders = interactable.GetComponents<Collider2D>();
            foreach (Collider2D collider in colliders)
            {
                collider.enabled = isClickable;
            }
        }
    }

    public void BlurBackground(bool active)
    {
        if (active)
        {
            secondaryCamera.GetComponent<Blur>().enabled = true;
            SetInteractableCollisions(false);
            inspectingSomething = true;
        }
        else
        {
            SetInteractableCollisions(true);
            secondaryCamera.GetComponent<Blur>().enabled = false;
            inspectingSomething = false;
        }
    }

    public Inspectable GetCurrentInspectable()
    {
        return currentInspectable;
    }

    public void SetCurrentInspectable(Inspectable newInspectable)
    {
        currentInspectable = newInspectable;
    }

    public bool GetInspectingSomething()
    {
        return inspectingSomething;
    }
}
