using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlashingArrow : MonoBehaviour
{
    private bool firstMovementDone;
    // Start is called before the first frame update
    void Start()
    {
        firstMovementDone = SceneLoadManager.GetInstance().savedRoom1State.firstCameraMovementDone;
        if (!firstMovementDone)
        {
            StartCoroutine(FlashArrow());
        }
        else
        {
            this.gameObject.SetActive(false);
        }
    }

    IEnumerator FlashArrow()
    {
        while(true)
        {
            this.GetComponent<SpriteRenderer>().enabled = false;
            yield return new WaitForSeconds(0.4f);
            this.GetComponent<SpriteRenderer>().enabled = true;
            yield return new WaitForSeconds(0.4f);
        }
    }

    public void StopFlashing()
    {
        firstMovementDone = true;
        SceneLoadManager.GetInstance().savedRoom1State.firstCameraMovementDone = true;
        StopAllCoroutines();
        this.gameObject.SetActive(false);
    }
}
