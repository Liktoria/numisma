using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextInspectable : Inspectable
{
    [SerializeField]
    protected GameObject textPrefab;
    protected GameObject instantiatedText;
    protected bool textIsShowing = false;

    protected override void InitializeSpecificAttributes()
    {
        textIsShowing = sceneLoadManager.gameState.textIsShowing;
    }

    public override void Search()
    {
        textIsShowing = sceneLoadManager.gameState.textIsShowing;
        if (!textIsShowing)
        {
            cameraPosition = new Vector3(cameraTransform.position.x, cameraTransform.position.y, 0.0f);
            interactionManager.GetCurrentInspectable().instantiatedModel.gameObject.layer = 6;
            instantiatedText = Instantiate(textPrefab, cameraPosition, Quaternion.identity);
            textIsShowing = true;
            sceneLoadManager.gameState.textIsShowing = true;
        }
    }

    public void HideText()
    {
            interactionManager.GetCurrentInspectable().instantiatedModel.layer = 0;
            Destroy(instantiatedText);
            textIsShowing = false;
            sceneLoadManager.gameState.textIsShowing = false;
    }
}
