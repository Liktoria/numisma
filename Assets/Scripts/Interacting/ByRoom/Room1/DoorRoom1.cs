using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorRoom1 : Door
{
    private bool canLeaveRoom = false;

    // Start is called before the first frame update
    protected override void InitializeSpecificAttributes()
    {
        canLeaveRoom = sceneLoadManager.savedRoom1State.canLeaveRoom;
    }

    //this overrides the OnMouseDown() function in Door
    void OnMouseDown()
    {
        canLeaveRoom = sceneLoadManager.savedRoom1State.canLeaveRoom;
        if (canLeaveRoom)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/door to hallway");
            UpdateCurrentRoom();
            sceneLoadManager.GoToScene(sceneToEnter);
        }
        else
        {
            dialogueManager.StartDialogue("DoorLocked");
        }
    }
}
