using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorBoard : Interactable
{
    private void OnMouseDown()
    {
        if(!sceneLoadManager.gameState.wearingCostume1 && !sceneLoadManager.gameState.wearingCostume2)
        {
            dialogueManager.StartDialogue("Floorboard");
        }
        else if(sceneLoadManager.gameState.wearingCostume1)
        {
            dialogueManager.StartDialogue("FloorboardDisguised");
        }
        else if(sceneLoadManager.gameState.wearingCostume2)
        {
            dialogueManager.StartDialogue("FloorboardDisguised2");
        }
    }
}
