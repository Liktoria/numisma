using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PokerTable : Interactable
{
    private void OnMouseDown()
    {
        if (!sceneLoadManager.gameState.wearingCostume1 && !sceneLoadManager.gameState.wearingCostume2)
        {
            dialogueManager.StartDialogue("PokerTable");
        }
        else if (sceneLoadManager.gameState.wearingCostume1)
        {
            dialogueManager.StartDialogue("PokerTableDisguised");
        }
        else if (sceneLoadManager.gameState.wearingCostume2)
        {
            dialogueManager.StartDialogue("PokerTableDisguised2");
        }
    }
}
