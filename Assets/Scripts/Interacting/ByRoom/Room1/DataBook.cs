using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataBook : Interactable
{
    private bool firstClickDone = false;
    [SerializeField]
    private GameObject bookButton;

    // Start is called before the first frame update
    protected override void InitializeSpecificAttributes()
    {
        firstClickDone = sceneLoadManager.savedRoom1State.collectedBook;
        if(firstClickDone)
        {
            bookButton.SetActive(true);
            Destroy(this.transform.gameObject);
        }
    }

    // Update is called once per frame
    private void OnMouseDown()
    {
        dialogueManager.StartDialogue("MatriculationRegister");
        Debug.Log("Uuuuh a book with numbers");
        sceneLoadManager.savedRoom1State.collectedBook = true;
        sceneLoadManager.CheckRoom1();
        bookButton.SetActive(true);
        Destroy(this.transform.gameObject);
    }
}
