using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coat : Inspectable
{
    private bool firstSearchDone = false;
    [SerializeField]
    private GameObject birthdayCardPrefab;
    [SerializeField]
    private GameObject birthdayCardInScene;
    private bool firstSearchDialogue = false;
    private bool firstTimeLooking = true;

    protected override void InitializeSpecificAttributes()
    {
        dialogueManager.Subscribe(this);
        firstSearchDone = sceneLoadManager.savedRoom1State.cardDiscovered;
        firstTimeLooking = !sceneLoadManager.savedRoom1State.firstCoatContactDone;

        if(firstSearchDone)
        {
            birthdayCardInScene.SetActive(true);
            firstSearchDialogue = false;
        }
    }

    void OnMouseDown()
    {
        ShowInspectable();
        firstTimeLooking = !sceneLoadManager.savedRoom1State.firstCoatContactDone;
        if (firstTimeLooking)
        {
            dialogueManager.StartDialogue("Coat1");
        }
    }

    protected override void DoAfterFade()
    {
        birthdayCardInScene.SetActive(true);
    }

    public override void Search()
    {
        firstSearchDone = sceneLoadManager.savedRoom1State.cardDiscovered;
        if (!firstSearchDone)
        {
            dialogueManager.StartDialogue("CoatSearch1");
            firstSearchDialogue = true;
        }
        else
        {
            if (!sceneLoadManager.gameState.wearingCostume1 && !sceneLoadManager.gameState.wearingCostume2)
            {
                dialogueManager.StartDialogue("CoatSearch2");
            }
            else if (sceneLoadManager.gameState.wearingCostume1)
            {
                dialogueManager.StartDialogue("CoatSearch2Disguised");
            }
            else if (sceneLoadManager.gameState.wearingCostume2)
            {
                dialogueManager.StartDialogue("CoatSearch2Disguised2");
            }
        }
    }

    public override void ReceiveDialogueFinished()
    {
        if(firstSearchDialogue)
        {
            RemoveInstanitatedModel();
            cameraPosition = new Vector3(cameraTransform.position.x, cameraTransform.position.y, 0.0f);
            instantiatedModel = Instantiate(birthdayCardPrefab, cameraPosition, Quaternion.identity);
            firstSearchDone = true;
            sceneLoadManager.savedRoom1State.cardDiscovered = true;
            sceneLoadManager.CheckRoom1();
            birthdayCardInScene.SetActive(true);
            firstSearchDialogue = false;
        }
    }
}
