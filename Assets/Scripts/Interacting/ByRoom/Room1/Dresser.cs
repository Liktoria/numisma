using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dresser : Interactable
{
    private bool firstInteractionDone = false;
    private bool talkedToJanitor = false;
    protected override void InitializeSpecificAttributes()
    {
        firstInteractionDone = sceneLoadManager.savedRoom1State.firstDresserContactDone;
        talkedToJanitor = sceneLoadManager.savedRoom1State.talkedToJanitor;
    }

    private void OnMouseDown()
    {
        if(!firstInteractionDone)
        {
            dialogueManager.StartDialogue("Cupboard1");
            firstInteractionDone = true; //at current state optional, but better safe than sorry
            sceneLoadManager.savedRoom1State.firstDresserContactDone = true;
            sceneLoadManager.CheckRoom1();
        }
        else if(talkedToJanitor)
        {
            if (!sceneLoadManager.gameState.wearingCostume1 && !sceneLoadManager.gameState.wearingCostume2)
            {
                dialogueManager.StartDialogue("Cupboard2");
                sceneLoadManager.gameState.wearingCostume1 = true;
                FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/dress up");
            }
        }
        else
        {
            if (!sceneLoadManager.gameState.wearingCostume1 && !sceneLoadManager.gameState.wearingCostume2)
            {
                dialogueManager.StartDialogue("Cupboard1");
            }
            else if (sceneLoadManager.gameState.wearingCostume1)
            {
                dialogueManager.StartDialogue("Cupboard1Disguised");
            }
            else if (sceneLoadManager.gameState.wearingCostume2)
            {
                dialogueManager.StartDialogue("Cupboard1Disguised2");
            }
        }
    }
}
