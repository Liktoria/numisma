using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitialDialogue : Interactable
{
    [SerializeField]
    private int roomNumber; // 0 = Hallway, 1 = Room1, 2 = Room2, 3 = Room3

    protected override void InitializeSpecificAttributes()
    {
        StartCoroutine(WaitALittle(1.0f));
    }

    IEnumerator WaitALittle(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        switch (roomNumber)
        {
            case 0:
                break;
            case 1:
                if (sceneLoadManager.savedRoom1State.firstEntering)
                {
                    dialogueManager.StartDialogue("FirstDialogue");
                    sceneLoadManager.savedRoom1State.firstEntering = false;
                }
                break;
            case 2:
                if (sceneLoadManager.savedRoom2State.firstEntering)
                {
                    dialogueManager.StartDialogue("FirstDialogue");
                    sceneLoadManager.savedRoom2State.firstEntering = false;
                }
                break;
            case 3:
                if (sceneLoadManager.savedRoom3State.firstEntering)
                {
                    dialogueManager.StartDialogue("FirstDialogue");
                    sceneLoadManager.savedRoom3State.firstEntering = false;
                }
                break;
            default:
                break;
        }
    }
}
