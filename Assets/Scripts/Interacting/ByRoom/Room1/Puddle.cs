using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puddle : Interactable
{
    private void OnMouseDown()
    {
        if (!sceneLoadManager.gameState.wearingCostume1 && !sceneLoadManager.gameState.wearingCostume2)
        {
            dialogueManager.StartDialogue("Puddle");
        }
        else if (sceneLoadManager.gameState.wearingCostume1)
        {
            dialogueManager.StartDialogue("PuddleDisguised");
        }
        else if (sceneLoadManager.gameState.wearingCostume2)
        {
            dialogueManager.StartDialogue("PuddleDisguised2");
        }
    }
}
