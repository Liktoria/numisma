using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chair : Interactable
{
    private void OnMouseDown()
    {
        if (!sceneLoadManager.gameState.wearingCostume1 && !sceneLoadManager.gameState.wearingCostume2)
        {
            dialogueManager.StartDialogue("Chair");
        }
        else if (sceneLoadManager.gameState.wearingCostume1)
        {
            dialogueManager.StartDialogue("ChairDisguised");
        }
        else if (sceneLoadManager.gameState.wearingCostume2)
        {
            dialogueManager.StartDialogue("ChairDisguised2");
        }
    }
}
