using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatPicture : Interactable
{
    private void OnMouseDown()
    {
        if (!sceneLoadManager.gameState.wearingCostume1 && !sceneLoadManager.gameState.wearingCostume2)
        {
            dialogueManager.StartDialogue("Picture");
        }
        else if (sceneLoadManager.gameState.wearingCostume1)
        {
            dialogueManager.StartDialogue("PictureDisguised");
        }
        else if (sceneLoadManager.gameState.wearingCostume2)
        {
            dialogueManager.StartDialogue("PictureDisguised2");
        }
    }
}
