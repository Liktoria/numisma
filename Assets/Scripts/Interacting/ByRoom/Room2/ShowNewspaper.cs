using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowNewspaper : MonoBehaviour
{
    [SerializeField]
    private GameObject newspaperPrefab;

    [SerializeField]
    private GameObject glasses;

    private GameObject instantiatedNewspaper;

    [System.NonSerialized]
    public bool paperIsShowing = false;

    private void OnMouseDown()
    {
        ShowPaper();
    }

    private void ShowPaper()
    {
        this.gameObject.layer = 6;
        instantiatedNewspaper = Instantiate(newspaperPrefab);
        paperIsShowing = true;
        this.GetComponent<Collider2D>().enabled = false;
        glasses.SetActive(false);
    }

    public void HideNewspaper()
    {
        this.gameObject.layer = 0;
        Destroy(instantiatedNewspaper);
        paperIsShowing = false;
        this.GetComponent<Collider2D>().enabled = true;
        glasses.SetActive(true);
    }
}
