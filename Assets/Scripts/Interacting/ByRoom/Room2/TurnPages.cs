using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TurnPages : Interactable
{
    private int currentPageIndex = 0;

    [SerializeField]
    private GameObject[] pages;

    [SerializeField]
    private GameObject[] texts;

    protected override void InitializeSpecificAttributes()
    {
        currentPageIndex = sceneLoadManager.savedRoom2State.currentInsectPage;

        for(int i = 0; i < pages.Length; i++)
        {
            if (i == currentPageIndex)
            {
                pages[i].SetActive(true);
                texts[i].SetActive(true);
            }
            else
            {
                pages[i].SetActive(false);
                texts[i].SetActive(false);
            }
        }
    }

    public void NextPage()
    {
        pages[currentPageIndex].SetActive(false);
        texts[currentPageIndex].SetActive(false);

        if (currentPageIndex < pages.Length - 1)
        {
            currentPageIndex++;            
        }

        pages[currentPageIndex].SetActive(true);
        texts[currentPageIndex].SetActive(true);
        sceneLoadManager.savedRoom2State.currentInsectPage = currentPageIndex;
    }

    public void PreviousPage()
    {
        pages[currentPageIndex].SetActive(false);
        texts[currentPageIndex].SetActive(false);

        if (currentPageIndex > 0)
        {
            currentPageIndex--;
        }

        pages[currentPageIndex].SetActive(true);
        texts[currentPageIndex].SetActive(true);
        sceneLoadManager.savedRoom2State.currentInsectPage = currentPageIndex;
    }
}
