using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pencil : Interactable
{
    private bool textDiscovered;
    private Vector3 startPosition;
    private Vector3 screenPoint;
    private Vector3 offset;
    private Vector3 oldMousePosition;
    private bool mouseIsMoving = false;

    protected override void InitializeSpecificAttributes()
    {
        textDiscovered = sceneLoadManager.savedRoom2State.textDiscovered;
        startPosition = transform.position;
    }
    private void OnMouseDown()
    {
        textDiscovered = sceneLoadManager.savedRoom2State.textDiscovered;
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }

    private void OnMouseDrag()
    {
        Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 cursorWithoutOffset = Camera.main.ScreenToWorldPoint(cursorPoint);
        Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
        Vector3 distanceToPrevious = cursorWithoutOffset - oldMousePosition;

        //TODO Evtentuell finetuning n�tig, um bugs zu vermeiden
        if (distanceToPrevious.magnitude >= 0.2f)
        {
            mouseIsMoving = true;
        }

        oldMousePosition = cursorWithoutOffset;
        transform.position = cursorPosition;
    }

    private void OnMouseUp()
    {
        StartCoroutine(MoveBack());

        if (!textDiscovered && !mouseIsMoving)
        {
            if(!sceneLoadManager.gameState.wearingCostume2)
            {
                dialogueManager.StartDialogue("Pencil1");
            }
            else
            {
                dialogueManager.StartDialogue("Pencil1Disguised");
            }
        }
        else if (textDiscovered && !mouseIsMoving)
        {
            if (!sceneLoadManager.gameState.wearingCostume2)
            {
                dialogueManager.StartDialogue("Pencil2");
            }
            else
            {
                dialogueManager.StartDialogue("Pencil2Disguised");
            }
        }
        mouseIsMoving = false;
    }

    IEnumerator MoveBack()
    {
        float timeSinceStart = 0f;

        while(true)
        {
            timeSinceStart += Time.deltaTime;
            transform.position = Vector3.Lerp(transform.position, startPosition, timeSinceStart);

            if (transform.position == startPosition)
            {
                yield break;
            }
            yield return null;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Notebook" && !textDiscovered)
        {
            collision.gameObject.GetComponent<Notebook>().SolveNotebook();
            textDiscovered = true;
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/pencil erasing");
            if (!sceneLoadManager.gameState.wearingCostume2)
            {
                dialogueManager.StartDialogue("NotebookWithPencil");
            }
            else
            {
                dialogueManager.StartDialogue("NotebookWithPencilDisguised");
            }
        }
    }
}
