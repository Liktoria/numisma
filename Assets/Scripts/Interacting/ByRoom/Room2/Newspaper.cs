using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Newspaper : Interactable
{
    private bool puzzleComplete;

    [SerializeField]
    private PuzzlePiece[] puzzlePieces = new PuzzlePiece[9];

    [SerializeField]
    private GameObject newspaperBorder;

    [SerializeField]
    private GameObject completeNewspaper;

    [SerializeField]
    private ParticleSystem particleSystem1;

    [SerializeField]
    private ParticleSystem particleSystem2;

    [SerializeField]
    private ParticleSystem particleSystem3;

    [SerializeField]
    private ParticleSystem particleSystem4;


    protected override void InitializeSpecificAttributes()
    {
        puzzleComplete = sceneLoadManager.savedRoom2State.puzzleComplete;
        if(puzzleComplete)
        {
            ShowCompletePuzzle();
        }
    }

    public void CheckPuzzleComplete()
    {
        foreach(PuzzlePiece puzzlePiece in puzzlePieces)
        {
            if(!puzzlePiece.positionCorrect)
            {
                puzzleComplete = false;
                break;
            }
            puzzleComplete = true;
        }
        sceneLoadManager.savedRoom2State.puzzleComplete = puzzleComplete;

        if (puzzleComplete)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/triangle");
            ShowCompletePuzzle();
            particleSystem1.Play();
            particleSystem2.Play();
            particleSystem3.Play();
            particleSystem4.Play();
        }
    }

    private void ShowCompletePuzzle()
    {
        newspaperBorder.SetActive(false);
        foreach (PuzzlePiece puzzlePiece in puzzlePieces)
        {
            puzzlePiece.gameObject.SetActive(false);
        }
        completeNewspaper.SetActive(true);
    }
}
