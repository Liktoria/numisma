using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallenChair : Interactable
{
    private void OnMouseDown()
    {
        if (!sceneLoadManager.gameState.wearingCostume2)
        {
            dialogueManager.StartDialogue("FallenChair");
        }
        else
        {
            dialogueManager.StartDialogue("FallenChairDisguised");
        }
    }
}
