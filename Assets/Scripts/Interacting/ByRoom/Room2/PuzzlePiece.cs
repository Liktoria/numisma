using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzlePiece : Interactable
{

    public bool positionCorrect = false;
    private Vector3 screenPoint;
    private Vector3 offset;
    private Vector3 vectorToCorrectPosition;
    private float maxDistance = 0.2f;
    private float distanceToCorrectPosition;

    [SerializeField]
    private Newspaper newspaper;

    [SerializeField]
    private Vector3 correctPosition = new Vector3(0, 0, 0);

    private void OnMouseDown()
    {
        screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/paper pick");
        offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
    }
    private void OnMouseDrag()
    {
        Vector3 cursorPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 cursorPosition = Camera.main.ScreenToWorldPoint(cursorPoint) + offset;
        transform.position = cursorPosition;
    }

    private void OnMouseUp()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/paper drop");
        vectorToCorrectPosition = correctPosition - transform.position;
        distanceToCorrectPosition = vectorToCorrectPosition.magnitude;

        if (CheckPosition())
        {
            newspaper.CheckPuzzleComplete();
        }
    }

    private bool CheckPosition()
    {
        if(distanceToCorrectPosition <= maxDistance)
        {
            positionCorrect = true;
        }
        else
        {
            positionCorrect = false;
        }

        return positionCorrect;
    }
}
