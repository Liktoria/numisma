using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pictures : Interactable
{
    private void OnMouseDown()
    {
        if (!sceneLoadManager.gameState.wearingCostume2)
        {
            dialogueManager.StartDialogue("Pictures");
        }
        else
        {
            dialogueManager.StartDialogue("PicturesDisguised");
        }
    }
}
