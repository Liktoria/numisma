using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindowRoom2 : Interactable
{
    private void OnMouseDown()
    {
        if (!sceneLoadManager.gameState.wearingCostume2)
        {
            dialogueManager.StartDialogue("Window");
        }
        else
        {
            dialogueManager.StartDialogue("WindowDisguised");
        }
    }
}
