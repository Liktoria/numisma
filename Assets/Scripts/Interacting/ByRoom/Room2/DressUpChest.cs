using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DressUpChest : Interactable
{
    void OnMouseDown()
    {
        if(!sceneLoadManager.gameState.wearingCostume2)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/dress up");
            sceneLoadManager.gameState.wearingCostume1 = false;
            sceneLoadManager.gameState.wearingCostume2 = true;
            dialogueManager.StartDialogue("Costume1");
        }
        else
        {
            dialogueManager.StartDialogue("Costume2");
        }
    }
}
