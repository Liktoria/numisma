using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Notebook : TextInspectable
{
    private bool textDiscovered = false;

    [SerializeField]
    private GameObject solvedNotebook;
    protected override void InitializeSpecificAttributes()
    {
        base.InitializeSpecificAttributes();
        textDiscovered = sceneLoadManager.savedRoom2State.textDiscovered;
        if (textDiscovered)
        {
            SolveNotebook();
        }
    }

    public override void Search()
    {
        InitializeSpecificAttributes();
        if (!textDiscovered)
        {
            if(!sceneLoadManager.gameState.wearingCostume2)
            {
                dialogueManager.StartDialogue("Notebook1");
            }
            else
            {
                dialogueManager.StartDialogue("Notebook1Disguised");
            }
        }
        else
        {
            base.Search();
        }
    }

    public void SolveNotebook()
    {
        closeUp = solvedNotebook;
        textDiscovered = true;
        sceneLoadManager.savedRoom2State.textDiscovered = true;
    }
}
