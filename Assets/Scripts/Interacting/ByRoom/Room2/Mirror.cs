using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mirror : Interactable
{
    private void OnMouseDown()
    {
        if(!sceneLoadManager.gameState.wearingCostume2)
        {
            dialogueManager.StartDialogue("Mirror");
        }
        else
        {
            dialogueManager.StartDialogue("MirrorDisguised");
        }
    }
}
