using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Books : Interactable
{
    private void OnMouseDown()
    {
        if(!sceneLoadManager.gameState.wearingCostume2)
        {
            dialogueManager.StartDialogue("Books");
        }
        else
        {
            dialogueManager.StartDialogue("BooksDisguised");
        }
    }
}
