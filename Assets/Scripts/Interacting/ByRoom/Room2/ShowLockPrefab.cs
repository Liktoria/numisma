using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowLockPrefab : MonoBehaviour
{
    [SerializeField]
    private GameObject lockPrefab;
    private GameObject instantiatedLock;

    [System.NonSerialized]
    public bool lockIsShowing = false;

    private void OnMouseDown()
    {
        ShowLock();
    }

    private void ShowLock()
    {
        this.gameObject.layer = 6;
        instantiatedLock = Instantiate(lockPrefab);
        lockIsShowing = true;
        this.GetComponent<Collider2D>().enabled = false;
    }

    public void HideLock()
    {
        this.gameObject.layer = 0;
        Destroy(instantiatedLock);
        lockIsShowing = false;
        this.GetComponent<Collider2D>().enabled = true;
    }
}
