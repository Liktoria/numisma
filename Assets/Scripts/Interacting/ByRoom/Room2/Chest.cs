using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : Inspectable
{
    [System.NonSerialized]
    public bool chestOpen;
    public bool chestInRoom2;

    public GameObject openChest;

    protected override void InitializeSpecificAttributes()
    {
        if (chestInRoom2)
        {
            chestOpen = sceneLoadManager.savedRoom2State.chestOpened;
        }
        else
        {
            chestOpen = sceneLoadManager.savedRoom3State.chestOpened;
        }

        if(chestOpen)
        {
            closeUp = openChest;
        }
    }

    public override void Search()
    {
        if(!chestOpen)
        {
            if (chestInRoom2)
            {
                dialogueManager.StartDialogue("Chest1");
            }
            else
            {
                dialogueManager.StartDialogue("Chest2");
            }
        }
        else
        {
            if (chestInRoom2)
            {
                if(sceneLoadManager.gameState.wearingCostume1)
                {
                    dialogueManager.StartDialogue("Chest2");
                }
                else
                {
                    dialogueManager.StartDialogue("Chest2Disguised");
                }
            }
            else
            {
                dialogueManager.StartDialogue("ChestOpen");
            }
        }
    }
}
