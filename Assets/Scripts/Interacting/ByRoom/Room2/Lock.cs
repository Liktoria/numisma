using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Lock : Interactable
{
    [SerializeField]
    private int[] correctCombination = { 1, 2, 3, 4, 5 };

    private int[] currentCombination;

    [SerializeField]
    private TMP_Text text1;
    [SerializeField]
    private TMP_Text text2;
    [SerializeField]
    private TMP_Text text3;
    [SerializeField]
    private TMP_Text text4;
    [SerializeField]
    private TMP_Text text5;

    [SerializeField]
    private GameObject canvas;

    private TMP_Text[] texts = new TMP_Text[5];
    private Chest chest;

    // Start is called before the first frame update
    protected override void InitializeSpecificAttributes()
    {
        chest = (Chest)interactionManager.GetCurrentInspectable();
        SetCanvasCamera();

        if (chest.chestInRoom2)
        {
            currentCombination = sceneLoadManager.savedRoom2State.currentCombination;
        }
        else
        {
           currentCombination = sceneLoadManager.savedRoom3State.currentCombination;
        }

        text1.text = "" + currentCombination[0];
        text2.text = "" + currentCombination[1];
        text3.text = "" + currentCombination[2];
        text4.text = "" + currentCombination[3];
        text5.text = "" + currentCombination[4];

        texts[0] = text1;
        texts[1] = text2;
        texts[2] = text3;
        texts[3] = text4;
        texts[4] = text5;
    }

    public void IncreaseDigit(int position)
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/lock roll");
        int currentValue = int.Parse(texts[position].text);

        if(0 <= currentValue && currentValue < 9)
        {
            currentValue++;
        }
        else
        {
            currentValue = 0;
        }

        currentCombination[position] = currentValue;
        UpdateSavedCombination(position, currentValue);

        texts[position].text = "" + currentCombination[position];

        if(CombinationCorrect())
        {
            UnlockChest();
        }
    }

    public void DecreaseDigit(int position)
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/lock roll");
        int currentValue = int.Parse(texts[position].text);

        if (0 < currentValue && currentValue <= 9)
        {
            currentValue--;
        }
        else
        {
            currentValue = 9;
        }

        currentCombination[position] = currentValue;
        UpdateSavedCombination(position, currentValue);

        texts[position].text = "" + currentCombination[position];

        if (CombinationCorrect())
        {
            UnlockChest();
        }
    }

    private void SetCanvasCamera()
    {
        canvas.GetComponent<Canvas>().worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    private void UnlockChest()
    {
        if (chest.chestInRoom2)
        {
            sceneLoadManager.savedRoom2State.chestOpened = true;
        }
        else
        {
            sceneLoadManager.savedRoom3State.chestOpened = true;
        }

        chest.chestOpen = true;
        chest.instantiatedModel.GetComponent<ShowLockPrefab>().HideLock();
        chest.RemoveInstanitatedModel();
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/lock unlock");
        chest.instantiatedModel = Instantiate(chest.openChest);
        chest.closeUp = chest.openChest;
        //dialogueManager.StartDialogue("Chest2");
    }

    private bool CombinationCorrect()
    {
        bool combinationCorrect = false;

        for(int i = 0; i < correctCombination.Length; i++)
        {
            if (currentCombination[i] != correctCombination[i])
            {
                combinationCorrect = false;
                break;
            }
            else
            {
                combinationCorrect = true;
            }
        }

        return combinationCorrect;
    }

    private void UpdateSavedCombination(int position, int value)
    {
        if (chest.chestInRoom2)
        {
            sceneLoadManager.savedRoom2State.currentCombination[position] = value;
        }
        else
        {
            sceneLoadManager.savedRoom3State.currentCombination[position] = value;
        }
    }
}
