using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Doll : Inspectable
{
    private bool clickable = false;
    protected override void InitializeSpecificAttributes()
    {
        clickable = sceneLoadManager.savedRoom3State.mirrorPuzzleSolved;
    }

    private void OnMouseDown()
    {
        clickable = sceneLoadManager.savedRoom3State.mirrorPuzzleSolved;
        if (clickable)
        {
            cameraPosition = new Vector3(cameraTransform.position.x, cameraTransform.position.y, 0.0f);
            currentlyInspecting = true;

            interactionManager.BlurBackground(true);
            interactionManager.SetCurrentInspectable(this);
            interactionManager.inspectMenuPanel.SetActive(true);
            instantiatedModel = Instantiate(closeUp, cameraPosition, Quaternion.identity);
        }
    }

    public override void Search()
    {
        Debug.Log("Some sort of puzzle...");
    }
}
