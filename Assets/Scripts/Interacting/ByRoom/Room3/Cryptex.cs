using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Cryptex : Interactable
{
    [SerializeField]
    private TMP_Text[] letters = new TMP_Text[8];

    [SerializeField]
    private GameObject canvas;

    //starting point: S, W, U, O, F, M, R, G
    //code to get to correct solution: 0, 3, 0, 9, 1, 8, 6, 1
    //correct solution: S, T, U, F, E, E, L, F

    private string[] characters = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

    private bool solutionCorrect = false;
    private string[] correctSolution = { "S", "T", "U", "F", "E", "E", "L", "F" };

    protected override void InitializeSpecificAttributes()
    {
        SetCanvasCamera();
        solutionCorrect = sceneLoadManager.savedRoom3State.cryptexSolutionCorrect;
        if(solutionCorrect)
        {
            for(int i = 0; i < letters.Length; i++)
            {
                letters[i].text = correctSolution[i];
            }
        }
    }

    public void DecreaseLetter(int position)
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/lock roll");
        int currentIndex = System.Array.IndexOf(characters, letters[position].text);
        if(currentIndex > 0)
        {
            currentIndex--;
        }
        else
        {
            currentIndex = characters.Length - 1;
        }

        letters[position].text = characters[currentIndex];

        if(CombinationCorrect())
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/lock unlock");
            solutionCorrect = true;
            sceneLoadManager.savedRoom3State.cryptexSolutionCorrect = true;
            sceneLoadManager.savedHallwayState.treasureDiscovered = true;
            dialogueManager.StartDialogue("CryptexSolved");
        }
    }

    private bool CombinationCorrect()
    {
        bool combinationCorrect = false;

        for(int i = 0; i < letters.Length; i++)
        {
            if(letters[i].text.Equals(correctSolution[i]))
            {
                combinationCorrect = true;
            }
            else
            {
                combinationCorrect = false;
                break;
            }
        }
        return combinationCorrect;
    }

    private void SetCanvasCamera()
    {
        canvas.GetComponent<Canvas>().worldCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
    }
}
