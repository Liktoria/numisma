using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MorseAlphabet : Interactable
{
    [SerializeField]
    private GameObject morsePrefab;
    private GameObject instantiatedMorseAlphabet;

    [System.NonSerialized]
    public bool morseIsShowing = false;

    private void OnMouseDown()
    {
        ShowMorseAlphabet();
    }

    private void ShowMorseAlphabet()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/paper");
        sceneLoadManager.savedRoom3State.morseAlphabetFound = true;
        this.gameObject.layer = 6;
        instantiatedMorseAlphabet = Instantiate(morsePrefab);
        morseIsShowing = true;
        this.GetComponent<Collider2D>().enabled = false;
    }

    public void HideMorseAlphabet()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/paper drop");
        this.gameObject.layer = 0;
        Destroy(instantiatedMorseAlphabet);
        morseIsShowing = false;
        this.GetComponent<Collider2D>().enabled = true;
    }
}
