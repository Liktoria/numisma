using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CheckMirrorInput : Interactable
{
    [SerializeField]
    private TMP_InputField mirrorInput;

    private GameObject doll;

    private string correctInput = "dieaugenderpuppe";

    [SerializeField]
    private Button Face1Button;

    [SerializeField]
    private Button Face2Button;

    [SerializeField]
    private Button Face3Button;

    [SerializeField]
    private Button Face4Button;

    [SerializeField]
    private Button PlayAllButton;

    [SerializeField]
    private Button StopButton;

    private MirrorMorseCode MirrorImages;

    protected override void InitializeSpecificAttributes()
    {
        MirrorImages = GameObject.Find("MirrorImages").GetComponent<MirrorMorseCode>();
        Face1Button.onClick.AddListener(delegate { ShowOneWord(1); });
        Face2Button.onClick.AddListener(delegate { ShowOneWord(2); });
        Face3Button.onClick.AddListener(delegate { ShowOneWord(3); });
        Face4Button.onClick.AddListener(delegate { ShowOneWord(4); });
        PlayAllButton.onClick.AddListener(FlashAll);
        StopButton.onClick.AddListener(StopFlashing);
    }

    private void StopFlashing()
    {
        MirrorImages.StopFlashing();
    }

    private void FlashAll()
    {
        MirrorImages.FlashImages();
    }

    private void ShowOneWord(int wordNumber)
    {
        switch (wordNumber)
        {
            case 1:
                MirrorImages.FlashWordOne();
                break;
            case 2:
                MirrorImages.FlashWordTwo();
                break;
            case 3:
                MirrorImages.FlashWordThree();
                break;
            case 4:
                MirrorImages.FlashWordFour();
                break;
        }
    }

    public void CheckInput()
    {
        string currentInput = mirrorInput.text;
        currentInput = currentInput.Replace(" ", string.Empty);
        currentInput = currentInput.ToLower();

        if(currentInput.Equals(correctInput))
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/triangle");
            dialogueManager.StartDialogue("MirrorSolved");
            sceneLoadManager.savedRoom3State.mirrorPuzzleSolved = true;
        }
        else
        {
            if(sceneLoadManager.savedRoom3State.morseAlphabetFound)
            {
                dialogueManager.StartDialogue("MirrorNotSolved2");
            }
            else
            {
                dialogueManager.StartDialogue("MirrorNotSolved1");
            }
        }
    }
}
