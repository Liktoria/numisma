using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorRoom3 : Inspectable
{
    private bool puzzleSolved;

    [SerializeField]
    private GameObject mirrorImages;

    protected override void InitializeSpecificAttributes()
    {
        puzzleSolved = sceneLoadManager.savedRoom3State.mirrorPuzzleSolved;
    }

    void OnMouseDown()
    {
        ShowInspectable();
        if(!sceneLoadManager.savedRoom3State.mirrorClicked)
        {
            dialogueManager.StartDialogue("Mirror");
            sceneLoadManager.savedRoom3State.mirrorClicked = true;
        }
    }

    public override void Search()
    {
        dialogueManager.StartDialogue("Mirror");
    }
}
