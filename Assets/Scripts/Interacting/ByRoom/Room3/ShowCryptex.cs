using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCryptex : Inspectable
{
    private bool combinationCorrect = false;

    protected override void InitializeSpecificAttributes()
    {
        combinationCorrect = sceneLoadManager.savedRoom3State.cryptexSolutionCorrect;
    }

    public override void Search()
    {
        combinationCorrect = sceneLoadManager.savedRoom3State.cryptexSolutionCorrect;
        if (!combinationCorrect)
        {
            dialogueManager.StartDialogue("CryptexFirst");
        }
        else
        {
            dialogueManager.StartDialogue("CryptexSolved2");
        }
    }
}
