using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MirrorMorseCode : Interactable
{
    [SerializeField]
    private GameObject Face1Short;

    [SerializeField]
    private GameObject Face2Short;

    [SerializeField]
    private GameObject Face3Short;

    [SerializeField]
    private GameObject Face4Short;

    [SerializeField]
    private GameObject Face1Long;
    [SerializeField]
    private GameObject Face2Long;
    [SerializeField]
    private GameObject Face3Long;
    [SerializeField]
    private GameObject Face4Long;


    [SerializeField]
    private float secondsShort = 0.25f;

    [SerializeField]
    private float secondsLong = 1.5f;

    [SerializeField]
    private float secondsSpace = 1.5f;

    [SerializeField]
    private float secondsLineBreak = 2.5f;

    [SerializeField]
    private float secondsNewFace = 2.5f;

    [System.NonSerialized]
    public bool displayAll = false;

    public void StopFlashing()
    {
        StopAllCoroutines();
        Face1Short.SetActive(false);
        Face2Short.SetActive(false);
        Face3Short.SetActive(false);
        Face4Short.SetActive(false);
        Face1Long.SetActive(false);
        Face2Long.SetActive(false);
        Face3Long.SetActive(false);
        Face4Long.SetActive(false);

        sceneLoadManager.savedRoom3State.mirrorFlashing = false;
        displayAll = false;
    }

    public void FlashImages()
    {
        if (!sceneLoadManager.savedRoom3State.mirrorFlashing)
        {
            StartCoroutine(WordOne());
            sceneLoadManager.savedRoom3State.mirrorFlashing = true;
            displayAll = true;
        }
    }

    public void FlashWordOne()
    {
        if (!sceneLoadManager.savedRoom3State.mirrorFlashing)
        {
            StartCoroutine(WordOne());
            sceneLoadManager.savedRoom3State.mirrorFlashing = true;
            displayAll = false;
        }
    }

    public void FlashWordTwo()
    {
        if (!sceneLoadManager.savedRoom3State.mirrorFlashing)
        {
            StartCoroutine(WordTwo());
            sceneLoadManager.savedRoom3State.mirrorFlashing = true;
            displayAll = false;
        }
    }

    public void FlashWordThree()
    {
        if (!sceneLoadManager.savedRoom3State.mirrorFlashing)
        {
            StartCoroutine(WordThree());
            sceneLoadManager.savedRoom3State.mirrorFlashing = true;
            displayAll = false;
        }
    }

    public void FlashWordFour()
    {
        if (!sceneLoadManager.savedRoom3State.mirrorFlashing)
        {
            StartCoroutine(WordFour());
            sceneLoadManager.savedRoom3State.mirrorFlashing = true;
            displayAll = false;
        }
    }

    private IEnumerator WordOne()
    {
        //long
        Face1Long.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsLong);
        Face1Long.SetActive(false);

        //space
        yield return new WaitForSecondsRealtime(secondsSpace);

        //short
        Face1Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face1Short.SetActive(false);

        //space
        yield return new WaitForSecondsRealtime(secondsSpace);

        //short
        Face1Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face1Short.SetActive(false);

        //new line -------------------------------------------------------------------------
        yield return new WaitForSecondsRealtime(secondsLineBreak);

        //short
        Face1Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face1Short.SetActive(false);

        //space

        yield return new WaitForSecondsRealtime(secondsSpace);

        //short
        Face1Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face1Short.SetActive(false);

        //new line --------------------------------------------------------------------------         
        yield return new WaitForSecondsRealtime(secondsLineBreak);

        //short
        Face1Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face1Short.SetActive(false);

        //new face ==============================================================================        
        yield return new WaitForSecondsRealtime(secondsNewFace);

        if(displayAll)
        {
            StartCoroutine(WordTwo());
        }
        else
        {
            StopFlashing();
        }
    }

    private IEnumerator WordTwo()
    {
        //short
        Face2Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face2Short.SetActive(false);

        //space         
        yield return new WaitForSecondsRealtime(secondsSpace);

        //long
        Face2Long.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsLong);
        Face2Long.SetActive(false);

        //new line --------------------------------------------------------------------------

        yield return new WaitForSecondsRealtime(secondsLineBreak);

        //short
        Face2Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face2Short.SetActive(false);

        //space        
        yield return new WaitForSecondsRealtime(secondsSpace);

        //short
        Face2Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face2Short.SetActive(false);

        //space        
        yield return new WaitForSecondsRealtime(secondsSpace);

        //long
        Face2Long.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsLong);
        Face2Long.SetActive(false);

        //new line --------------------------------------------------------------------------       
        yield return new WaitForSecondsRealtime(secondsLineBreak);

        //long
        Face2Long.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsLong);
        Face2Long.SetActive(false);

        //space        
        yield return new WaitForSecondsRealtime(secondsSpace);

        //long
        Face2Long.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsLong);
        Face2Long.SetActive(false);

        //space        
        yield return new WaitForSecondsRealtime(secondsSpace);

        //short
        Face2Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face2Short.SetActive(false);

        //new line --------------------------------------------------------------------------        
        yield return new WaitForSecondsRealtime(secondsLineBreak);

        //short
        Face2Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face2Short.SetActive(false);

        //new line --------------------------------------------------------------------------        
        yield return new WaitForSecondsRealtime(secondsLineBreak);

        //long
        Face2Long.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsLong);
        Face2Long.SetActive(false);

        //space        
        yield return new WaitForSecondsRealtime(secondsSpace);

        //short
        Face2Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face2Short.SetActive(false);

        //new face ==============================================================================         
        yield return new WaitForSecondsRealtime(secondsNewFace);
        if (displayAll)
        {
            StartCoroutine(WordThree());
        }
        else
        {
            StopFlashing();
        }
    }

    private IEnumerator WordThree()
    {
        //long
        Face3Long.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsLong);
        Face3Long.SetActive(false);

        //space         
        yield return new WaitForSecondsRealtime(secondsSpace);

        //short
        Face3Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face3Short.SetActive(false);

        //space

        yield return new WaitForSecondsRealtime(secondsSpace);

        //short
        Face3Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face3Short.SetActive(false);

        //new line --------------------------------------------------------------------------
        yield return new WaitForSecondsRealtime(secondsLineBreak);

        //short
        Face3Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face3Short.SetActive(false);

        //new line --------------------------------------------------------------------------
        yield return new WaitForSecondsRealtime(secondsLineBreak);

        //short
        Face3Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face3Short.SetActive(false);

        //space
        yield return new WaitForSecondsRealtime(secondsSpace);

        //long
        Face3Long.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsLong);
        Face3Long.SetActive(false);

        //space
        yield return new WaitForSecondsRealtime(secondsSpace);

        //short
        Face3Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face3Short.SetActive(false);

        //new face ==============================================================================
        yield return new WaitForSecondsRealtime(secondsNewFace);

        if (displayAll)
        {
            StartCoroutine(WordFour());
        }
        else
        {
            StopFlashing();
        }
    }

    private IEnumerator WordFour()
    {
        //short
        Face4Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face4Short.SetActive(false);

        //space
        yield return new WaitForSecondsRealtime(secondsSpace);

        //long
        Face4Long.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsLong);
        Face4Long.SetActive(false);

        //space
        yield return new WaitForSecondsRealtime(secondsSpace);

        //long
        Face4Long.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsLong);
        Face4Long.SetActive(false);

        //space
        yield return new WaitForSecondsRealtime(secondsSpace);

        //short
        Face4Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face4Short.SetActive(false);

        //new line --------------------------------------------------------------------------
        yield return new WaitForSecondsRealtime(secondsLineBreak);

        //short
        Face4Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face4Short.SetActive(false);

        //space
        yield return new WaitForSecondsRealtime(secondsSpace);

        //short
        Face4Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face4Short.SetActive(false);

        //space
        yield return new WaitForSecondsRealtime(secondsSpace);

        //long
        Face4Long.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsLong);
        Face4Long.SetActive(false);

        //new line --------------------------------------------------------------------------
        yield return new WaitForSecondsRealtime(secondsLineBreak);

        //short
        Face4Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face4Short.SetActive(false);

        //space
        yield return new WaitForSecondsRealtime(secondsSpace);

        //long
        Face4Long.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsLong);
        Face4Long.SetActive(false);

        //space
        yield return new WaitForSecondsRealtime(secondsSpace);

        //long
        Face4Long.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsLong);
        Face4Long.SetActive(false);

        //space
        yield return new WaitForSecondsRealtime(secondsSpace);

        //short
        Face4Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face4Short.SetActive(false);

        //new line --------------------------------------------------------------------------
        yield return new WaitForSecondsRealtime(secondsLineBreak);

        //short
        Face4Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face4Short.SetActive(false);

        //space
        yield return new WaitForSecondsRealtime(secondsSpace);

        //long
        Face4Long.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsLong);
        Face4Long.SetActive(false);

        //space
        yield return new WaitForSecondsRealtime(secondsSpace);

        //long
        Face4Long.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsLong);
        Face4Long.SetActive(false);

        //space
        yield return new WaitForSecondsRealtime(secondsSpace);

        //short
        Face4Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face4Short.SetActive(false);

        //new line --------------------------------------------------------------------------
        yield return new WaitForSecondsRealtime(secondsLineBreak);

        //short
        Face4Short.SetActive(true);
        yield return new WaitForSecondsRealtime(secondsShort);
        Face4Short.SetActive(false);

        StopFlashing();
    }
}
