using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class PoetryPuzzle : Interactable
{
    private bool firstHintDone;
    private bool secondHintDone;
    [SerializeField]
    private GameObject highlightedTexts;
    [SerializeField]
    private TMP_InputField inputDay;
    [SerializeField]
    private TMP_InputField inputMonth;
    [SerializeField]
    private TMP_InputField inputYear;

    [SerializeField]
    private TMP_Text hintButtonText;

    private bool solutionCorrect;


    protected override void InitializeSpecificAttributes()
    {
        solutionCorrect = sceneLoadManager.savedRoom3State.poetryPuzzleSolved;
        if(sceneLoadManager.savedRoom3State.poetryHintShown)
        {
            firstHintDone = true;
            secondHintDone = true;
            highlightedTexts.SetActive(true);
        }
        if(solutionCorrect)
        {
            inputDay.text = "03";
            inputMonth.text = "09";
            inputYear.text = "1861";
        }
    }

    public void ShowHint()
    {
        if(!firstHintDone)
        {
            firstHintDone = true;
            dialogueManager.StartDialogue("PoetryPuzzleFirstHint");
            hintButtonText.text = "Tipp 2";
        }
        else if(!secondHintDone)
        {
            secondHintDone = true;
            dialogueManager.StartDialogue("PoetryPuzzleSecondHint");
            hintButtonText.text = "Letzter Tipp";
        }
        else
        {
            highlightedTexts.SetActive(true);
            sceneLoadManager.savedRoom3State.poetryHintShown = true;
        }
    }

    public void CheckSolution()
    {
        string cleanedDay = inputDay.text.Replace(" ", string.Empty);
        string cleanedMonth = inputMonth.text.Replace(" ", string.Empty);
        string cleanedYear = inputYear.text.Replace(" ", string.Empty);

        if (cleanedDay == "03" && cleanedMonth == "09" && cleanedYear == "1861")
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/triangle");
            solutionCorrect = true;
            sceneLoadManager.savedRoom3State.poetryPuzzleSolved = true;
            dialogueManager.StartDialogue("PoetryPuzzleSolved");
        }
        else
        {
            dialogueManager.StartDialogue("PoetryPuzzleNotSolved");
        }
    }
}
