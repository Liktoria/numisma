using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Phone : Interactable
{
    private void OnMouseDown()
    {
        dialogueManager.StartDialogue("Phone");
    }
}
