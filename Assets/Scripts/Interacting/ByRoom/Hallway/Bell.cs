using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bell : Inspectable
{
    private bool firstBellInteraction;
    private bool showInputField;
    private bool dialogueBell1;
    private bool dialogueBell2;
    private bool dialogueBell3;

    protected override void InitializeSpecificAttributes()
    {
        CheckValues();
        dialogueManager.Subscribe(this);
    }
    void OnMouseDown()
    {
        CheckValues();

        if (firstBellInteraction)
        {
            dialogueManager.StartDialogue("Bell1");
            sceneLoadManager.savedRoom1State.talkedToJanitor = true;
            firstBellInteraction = false;
            sceneLoadManager.savedHallwayState.firstBellInteraction = false;
            dialogueBell1 = true;
        }
        else if(sceneLoadManager.gameState.wearingCostume1 && !sceneLoadManager.savedHallwayState.doorRoom2Open)
        {
            dialogueManager.StartDialogue("Bell2");
            dialogueBell2 = true;
        }
        else if(sceneLoadManager.gameState.wearingCostume2 && !sceneLoadManager.savedHallwayState.doorRoom3Open)
        {
            dialogueManager.StartDialogue("Bell3");
            dialogueBell3 = true;
        }
        else
        {
            dialogueManager.StartDialogue("BellWithoutCostume");
        }
    }

    private void CheckValues()
    {
        firstBellInteraction = sceneLoadManager.savedHallwayState.firstBellInteraction;
        showInputField = dialogueBell2 || dialogueBell3;
    }

    public override void ReceiveDialogueFinished()
    {
        CheckValues();

        if(dialogueBell1)
        {
            StartCoroutine(StartThinking(0.8f));
            dialogueBell1 = false;
        }
        else if(showInputField)
        {
            interactionManager.BlurBackground(true);
            interactionManager.SetCurrentInspectable(this);
            interactionManager.inspectMenuPanel.SetActive(true);
            instantiatedModel = Instantiate(closeUp);
            dialogueBell2 = false;
            dialogueBell3 = false;
        }
    }

    IEnumerator StartThinking(float delay)
    {
        yield return new WaitForSeconds(delay);
        dialogueManager.StartDialogue("ThinkingAfterBell");
    }
}
