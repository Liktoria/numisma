using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stairs : Inspectable
{
    private bool treasureFound = false;
    private bool dialogueStairs = false;
    private bool lastDialogue = false;

    protected override void InitializeSpecificAttributes()
    {
        treasureFound = sceneLoadManager.savedHallwayState.treasureDiscovered;
        dialogueManager.Subscribe(this);
    }

    private void OnMouseDown()
    {
        treasureFound = sceneLoadManager.savedHallwayState.treasureDiscovered;

        if (treasureFound)
        {
            dialogueManager.StartDialogue("Stairs");
            dialogueStairs = true;
        }
    }

    public override void ReceiveDialogueFinished()
    {
        if(dialogueStairs)
        {
            cameraPosition = new Vector3(cameraTransform.position.x, cameraTransform.position.y, 0.0f);
            currentlyInspecting = true;

            interactionManager.BlurBackground(true);
            interactionManager.SetCurrentInspectable(this);
            interactionManager.inspectMenuPanel.SetActive(true);
            instantiatedModel = Instantiate(closeUp, cameraPosition, Quaternion.identity);
            dialogueStairs = false;
        }
        else if(lastDialogue)
        {
            sceneLoadManager.GoToScene("WinScreen");
            lastDialogue = false;
        }
    }

    public void StartLastDialogue()
    {
        dialogueManager.StartDialogue("LastDialogue");
        lastDialogue = true;
    }
}
