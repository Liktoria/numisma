using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorRoom2 : Door
{
    private bool doorOpen = true;

    protected override void InitializeSpecificAttributes()
    {
        doorOpen = sceneLoadManager.savedHallwayState.doorRoom2Open;
    }
    private void OnMouseDown()
    {
        doorOpen = sceneLoadManager.savedHallwayState.doorRoom2Open;

        if (doorOpen)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/door to inside");
            UpdateCurrentRoom();
            sceneLoadManager.GoToScene(sceneToEnter);
        }
        else
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/door locked");
            if (sceneLoadManager.gameState.wearingCostume1)
            {
                dialogueManager.StartDialogue("Door2WithCostume");
            }
            else
            {
                dialogueManager.StartDialogue("Door2LockedWithoutCostume");
            }

        }
    }
}
