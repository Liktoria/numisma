using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Janitor : Interactable
{

    private void OnMouseDown()
    {
        if(sceneLoadManager.gameState.wearingCostume1 && !sceneLoadManager.savedHallwayState.doorRoom2Open)
        {
            dialogueManager.StartDialogue("JanitorDoorWithCostume1");
        }
        else if(!sceneLoadManager.savedHallwayState.doorRoom2Open)
        {
            dialogueManager.StartDialogue("JanitorDoor1");
        }
    }
}
