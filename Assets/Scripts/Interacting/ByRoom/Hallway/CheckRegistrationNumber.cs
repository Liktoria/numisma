using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CheckRegistrationNumber : Interactable
{
    [SerializeField]
    private TMP_InputField inputField;

    private string correctNumber1 = "2814";
    private string correctNumber2 = "1875";

    public void CheckNumber()
    {
        string cleanedInput = inputField.text.Replace(" ", string.Empty);
        if(!sceneLoadManager.savedHallwayState.doorRoom2Open)
        { 
            if(cleanedInput == correctNumber1)
            {
                FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/keys");
                dialogueManager.StartDialogue("RightRegistrationNumber");
                sceneLoadManager.savedHallwayState.doorRoom2Open = true;
                interactionManager.BackToSceneView();
            }
            else
            {
                dialogueManager.StartDialogue("WrongRegistrationNumber1");
                interactionManager.BackToSceneView();
            }
        }
        else
        {
            if (cleanedInput == correctNumber2)
            {
                FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/keys");
                dialogueManager.StartDialogue("RightRegistrationNumber");
                sceneLoadManager.savedHallwayState.doorRoom3Open = true;
                interactionManager.BackToSceneView();
            }
            else
            {
                dialogueManager.StartDialogue("WrongRegistrationNumber2");
                interactionManager.BackToSceneView();
            }
        }
    }
}
