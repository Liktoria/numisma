using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorRoom3 : Door
{
    private bool doorOpen = true;

    protected override void InitializeSpecificAttributes()
    {
        doorOpen = sceneLoadManager.savedHallwayState.doorRoom3Open;
    }

    private void OnMouseDown()
    {
        doorOpen = sceneLoadManager.savedHallwayState.doorRoom3Open;

        if(doorOpen)
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/door to inside");
            UpdateCurrentRoom();
            sceneLoadManager.GoToScene(sceneToEnter);
        }
        else
        {
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/door locked");
            if(sceneLoadManager.savedHallwayState.doorRoom2Open)
            {
                dialogueManager.StartDialogue("Room3AfterRoom2");
            }
            else
            {
                dialogueManager.StartDialogue("Room3BeforeRoom2");
            }
        }
    }
}
