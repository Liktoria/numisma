using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inspectable : Interactable
{
    //The 3D model of the object
    [SerializeField]
    [Tooltip("Drag the close up prefab here")]
    public GameObject closeUp;

    //[SerializeField]
    //protected string dialogueOnFirstClick = "";

    //Is an object currently being inspected?
    [System.NonSerialized]
    public bool currentlyInspecting = false;

    [System.NonSerialized]
    public GameObject instantiatedModel;
    protected bool firstInteraction = true;

    public delegate void InspectAction(GameObject o);
    public event InspectAction OnInspect;

    void OnMouseDown()
    {
        ShowInspectable();
    }

    public void RemoveInstanitatedModel()
    {
        Destroy(instantiatedModel);
    }

    public virtual void Search()
    {

    }

    public void ShowInspectable()
    {
        cameraPosition = new Vector3(cameraTransform.position.x, cameraTransform.position.y, 0.0f);
        currentlyInspecting = true;

        OnInspect?.Invoke(closeUp);

        interactionManager.BlurBackground(true);
        interactionManager.SetCurrentInspectable(this);
        interactionManager.inspectMenuPanel.SetActive(true);
        instantiatedModel = Instantiate(closeUp, cameraPosition, Quaternion.identity);
    }
}
