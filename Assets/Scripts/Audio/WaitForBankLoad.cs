using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WaitForBankLoad : MonoBehaviour
{
    [FMODUnity.BankRef] public string bank;

    public UnityEvent onLoad;

    void Start()
    {
        StartCoroutine(CheckLoaded());
    }

    IEnumerator CheckLoaded()
    {
        while (!FMODUnity.RuntimeManager.HasBankLoaded(bank))
        {
            yield return null;
        }
        onLoad?.Invoke();
    }
}
