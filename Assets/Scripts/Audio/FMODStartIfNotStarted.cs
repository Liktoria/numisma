using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FMODStartIfNotStarted : MonoBehaviour
{
    public UnityEvent actionsToDo;

    public void StartIfNotStarted()
    {
        if (Application.platform == RuntimePlatform.WebGLPlayer || Application.isEditor)
        {
            FMODUnity.RuntimeManager.CoreSystem.mixerSuspend();
            FMODUnity.RuntimeManager.CoreSystem.mixerResume();
            actionsToDo?.Invoke();
        }
    }
}
