using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SetButtonsSFX : MonoBehaviour
{
    [FMODUnity.EventRef]
    public string audioEvent;    

    void Start()
    {
        SetSFX();
        SceneManager.activeSceneChanged += (prev, next) => SetSFX();
    }

    void SetSFX()
    {
        var buttons = FindObjectsOfType<Button>(true);
        foreach (var o in buttons)
        {
            o.onClick.AddListener(() => FMODUnity.RuntimeManager.PlayOneShot(audioEvent));
        }
    }
}
