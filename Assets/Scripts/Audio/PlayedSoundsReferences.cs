using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayedSoundsReferences : Singleton<PlayedSoundsReferences>
{
    protected PlayedSoundsReferences(){} // Prevent new PlayedSoundsReferences()

    List<string> playedSounds = new List<string>();    

    public void Add(string sound)
    {
        if (!playedSounds.Contains(sound)) playedSounds.Add(sound);
    }        

    public bool HasPlayed(string sound)
    {
        return playedSounds.Contains(sound);
    }
}
