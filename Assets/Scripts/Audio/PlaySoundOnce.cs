using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlaySoundOnce : MonoBehaviour
{
    [FMODUnity.EventRefAttribute]
    public string audioEvent;

    void Start()
    {
        if (!PlayedSoundsReferences.Instance.HasPlayed(audioEvent))
            FMODUnity.RuntimeManager.PlayOneShot(audioEvent);
        PlayedSoundsReferences.Instance.Add(audioEvent);
    }
}
