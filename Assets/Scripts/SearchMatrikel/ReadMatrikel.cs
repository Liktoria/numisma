using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using TMPro;

//['Nummer', 'Datum', 'Familienname', 'Vorname(n)', 'Studium','Geburts-datum', 'Geburtsort', 'Familienname Vater/Mutter','Vorname Vater/Mutter', 'Titel/Beruf/Stand Vater/Mutter','Wohnort Vater/Mutter']

public class ReadMatrikel : MonoBehaviour
{
    // 2 = Nachname
    // 3 = Vorname
    // 4 = Studiengang
    // 5 = Geburstag
    // 6 = Geburtsort
    public TMP_InputField vorname;
    public TMP_InputField nachname;
    public TMP_InputField studiengang;
    public TMP_InputField geburstag;
    public TMP_InputField geburtsort;
    public GameObject rowPrefab;
    public GameObject content;
    private bool searchDone = false;

    // Function to check if string is in another string
    bool Equal(string a, string b)
    {
        return (a.ToLower().IndexOf(b.ToLower()) > -1);
    }
    // Filter function for two string arrayy
    bool FilterArrays(string[] row, string[] filter)
    {

        bool flag = true;

        // Loop over array of input values, check if given values are equal to row values
        for (int i = 0; i <= filter.Length - 1; i++)
        {
            // Check for empty strings as input
            if (String.IsNullOrEmpty(filter[i]))
            {
                continue;
            }
            else
            {
                //check if filter string and string in row
                if (Equal(row[i], filter[i]))
                {
                    continue;
                }
                else
                {
                    // changes flags if some search strings not same as row values
                    flag = false;
                }
            };
        };
        return flag;
    }
    public void Search()
    {
        string text_vorname = vorname.text;
        string text_nachname = nachname.text;
        string text_studiengang = studiengang.text;
        string text_geburtstag = geburstag.text;
        string text_geburtsort = geburtsort.text;

        //filter array with given inputs
        string[] filterarray = new string[5] { text_nachname, text_vorname, text_studiengang, text_geburtstag, text_geburtsort };

        //Iterating over rows in data.
        TextAsset matrikeldata = Resources.Load<TextAsset>("Matrikeldaten");
        string[] data = matrikeldata.text.Split(new char[] { '\n' });

        for (int i = 1; i < data.Length - 1; i++)
        {   
            if(searchDone)
            {
                foreach(Transform child in content.transform)
                {
                    Destroy(child.gameObject);
                }
                searchDone = false;
            }
            //Splitting up the row, into an array for better filtering.
            string[] row = data[i].Split(new char[] { '$' });

            string[] searcharray = new string[5] { row[2], row[3], row[4], row[5], row[6] };
            if (FilterArrays(searcharray, filterarray))
            {
                GameObject instantiatedRow = Instantiate(rowPrefab, content.transform);
                for(int j = 0; j < row.Length; j++)
                {
                    instantiatedRow.GetComponent<Row>().textFields[j].text = row[j];
                }
                Debug.Log(String.Join(" ", row));
            };
        };
        Debug.Log("Fertig");
        searchDone = true;
    }
}
