using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    [SerializeField]
    private float rightLimit;

    [SerializeField]
    private float movementSpeed;

    [SerializeField]
    private float edgeArea;

    [SerializeField]
    private FlashingArrow arrow;

    private bool firstMovement = true;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(0.0f, 0.0f, -10.0f);
        firstMovement = !SceneLoadManager.GetInstance().savedRoom1State.firstCameraMovementDone;
    }

    // Update is called once per frame
    void Update()
    {
        if (!InteractionManager.GetInstance().GetInspectingSomething())
        {
            if (Input.mousePosition.x <= 0 + edgeArea)
            {
                if (0 < transform.position.x)
                {
                    transform.position += Vector3.left * Time.deltaTime * movementSpeed;
                }
            }
            else if (Input.mousePosition.x >= Screen.width - edgeArea)
            {
                if (transform.position.x < rightLimit)
                {
                    transform.position += Vector3.right * Time.deltaTime * movementSpeed;
                    arrow.StopFlashing();
                }
            }
        }
    }
}
