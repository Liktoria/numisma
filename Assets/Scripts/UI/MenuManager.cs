using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public bool CreditsOpen
    {
        get { return creditsOpen; }
        set
        {
            creditCanvas.gameObject.SetActive(value);
            creditsOpen = value;
        }
    }

    private bool creditsOpen = false;

    [SerializeField]
    private Canvas creditCanvas = null;
}
