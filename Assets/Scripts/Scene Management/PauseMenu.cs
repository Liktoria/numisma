using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject pauseMenu;

    private bool pauseMenuShowing = false;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            if(!pauseMenuShowing)
            {
                pauseMenu.SetActive(true);
                pauseMenuShowing = true;
                InteractionManager.GetInstance().BlurBackground(true);
            }
            else
            {
                HidePausemenu();
            }
            
        }
    }
    
    public void QuitGame()
    {
        SceneLoadManager.GetInstance().QuitGame();
    }

    public void HidePausemenu()
    {
        pauseMenu.SetActive(false);
        InteractionManager.GetInstance().BlurBackground(false);
        pauseMenuShowing = false;
    }

    public void BackToMenu()
    {
        SceneLoadManager.GetInstance().GoToSceneByIndex(0);
    }
}
