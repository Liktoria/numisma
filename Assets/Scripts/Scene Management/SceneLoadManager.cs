using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoadManager : MonoBehaviour
{
    private static SceneLoadManager instance;
    public Room1State savedRoom1State = new Room1State();
    public HallwayState savedHallwayState = new HallwayState();
    public Room2State savedRoom2State = new Room2State();
    public Room3State savedRoom3State = new Room3State();
    public State gameState = new State();

    private void Awake()
    {
        if (instance == null)
        {
            DontDestroyOnLoad(gameObject);
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
    }
    public static SceneLoadManager GetInstance()
    {
        return instance;
    }

    private SceneLoadManager() { }

    public void GoToScene(string sceneName)
    {
        TransitionScene transitionScript = GameObject.Find("TransitionManager").GetComponent<TransitionScene>();
        transitionScript.FadeOut(sceneName);
    }

    public void GoToSceneByIndex(int sceneIndex)
    {
        TransitionScene transitionScript = GameObject.Find("TransitionManager").GetComponent<TransitionScene>();
        transitionScript.FadeOut(sceneIndex);
    }
    
    public void CheckRoom1()
    {
        if(savedRoom1State.collectedBook && savedRoom1State.cardDiscovered && savedRoom1State.firstDresserContactDone)
        {
            savedRoom1State.canLeaveRoom = true;
        }
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
