using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TransitionScene : MonoBehaviour
{
    [SerializeField]
    private Image fadeImage;

    [SerializeField]
    private GameObject fadeCanvas;

    [SerializeField]
    private bool transitionOnStart = true;

    [SerializeField]
    private float fadeDuration = 1.0f;

    private float transitionTime = 10.0f;

    [Tooltip ("Only needs to contain something, when transitionOnStart is true")]
    [SerializeField]
    private string nextScene;

    private int nextSceneIndex = 0;

    [SerializeField]
    private bool fade = true;

    private Color imageColor;
    private Color nonTransparentBlack;
    private Color transparentBlack;
    private bool leaveScene = false;
    private bool sceneByIndex = false;

    // Start is called before the first frame update
    void Start()
    {
        imageColor = fadeImage.color;
        imageColor.a = 0;
        transparentBlack = imageColor;
        imageColor.a = 1;
        nonTransparentBlack = imageColor;
        leaveScene = false;

        if (transitionOnStart && fade)
        {
            StartCoroutine(FadeFunction(nonTransparentBlack.a, transparentBlack.a, fadeDuration));
            StartCoroutine(WaitForTransition());
        }
        else if(fade && !transitionOnStart)
        {
            StartCoroutine(FadeFunction(nonTransparentBlack.a, transparentBlack.a, fadeDuration));
        }
    }

    IEnumerator WaitForTransition()
    {
        yield return new WaitForSeconds(transitionTime - fadeDuration);
        leaveScene = true;
        StartCoroutine(FadeFunction(transparentBlack.a, nonTransparentBlack.a, fadeDuration));
    }

    IEnumerator FadeFunction(float startValue, float endValue, float duration)
    {
        float time = 0;
        fadeCanvas.SetActive(true);

        if(!(SceneManager.GetActiveScene().name == "Menu"
            || SceneManager.GetActiveScene().name == "Disclaimer"
            || SceneManager.GetActiveScene().name == "GameExplanation"
            || SceneManager.GetActiveScene().name == "WinScreen"))
        {
            InteractionManager.GetInstance().SetInteractableCollisions(false);
        }
        
        while (time < duration)
        {
            imageColor.a = Mathf.Lerp(startValue, endValue, time / duration);
            fadeImage.color = imageColor;

            time += Time.deltaTime;
            yield return null;
        }
        imageColor.a = endValue;
        fadeImage.color = imageColor;

        if (endValue == transparentBlack.a)
        {
            fadeCanvas.SetActive(false);

            if (!(SceneManager.GetActiveScene().name == "Menu"
            || SceneManager.GetActiveScene().name == "Disclaimer"
            || SceneManager.GetActiveScene().name == "GameExplanation"
            || SceneManager.GetActiveScene().name == "WinScreen"))
            {
                InteractionManager.GetInstance().SetInteractableCollisions(true);
            }
        }

        if(leaveScene)
        {
            if(sceneByIndex)
            {
                SceneManager.LoadScene(nextSceneIndex);
            }
            else
            {
                SceneManager.LoadScene(nextScene);
            }
        }
    }

    public void FadeIn()
    {
        StartCoroutine(FadeFunction(nonTransparentBlack.a, transparentBlack.a, fadeDuration));
    }

    public void FadeOut(string newScene)
    {
        leaveScene = true;
        nextScene = newScene;
        sceneByIndex = false;
        StartCoroutine(FadeFunction(transparentBlack.a, nonTransparentBlack.a, fadeDuration));
    }

    public void FadeOut(int sceneIndex)
    {
        leaveScene = true;
        nextSceneIndex = sceneIndex;
        sceneByIndex = true;
        StartCoroutine(FadeFunction(transparentBlack.a, nonTransparentBlack.a, fadeDuration));
    }
}
