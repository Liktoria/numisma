using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoad : MonoBehaviour
{
    void OnEnable()
    {
        var objs = FindObjectsOfType<DontDestroyOnLoad>();
        foreach (var o in objs)
        {
            if (o.gameObject != this.gameObject && o.gameObject.name == this.name)
            {
                Destroy(this.gameObject);
                return;
            }
        }
        DontDestroyOnLoad(this);
    }
}
