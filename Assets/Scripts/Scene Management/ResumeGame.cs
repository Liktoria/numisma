using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ResumeGame : Interactable
{
    [SerializeField]
    private TransitionScene transitionScript;
    [SerializeField]
    private TMP_Text buttonText;

    protected override void InitializeSpecificAttributes()
    {
        if(sceneLoadManager.gameState.gameStartedOnce)
        {
            buttonText.text = "Spiel fortsetzen";
        }
    }

    public void ButtonClicked()
    {
        if(!sceneLoadManager.gameState.gameStartedOnce)
        {
            sceneLoadManager.gameState.gameStartedOnce = true;
            transitionScript.FadeOut("Disclaimer");
            buttonText.text = "Spiel fortsetzen";
        }
        else
        {
            switch(sceneLoadManager.gameState.currentRoom)
            {
                case 0:
                    transitionScript.FadeOut("Hallway");
                    break;
                case 1:
                    transitionScript.FadeOut("Room1");
                    break;
                case 2:
                    transitionScript.FadeOut("Room2");
                    break;
                case 3:
                    transitionScript.FadeOut("Room3");
                    break;
                case 4:
                    transitionScript.FadeOut("Storeroom");
                    break;
            }
        }
    }
}
