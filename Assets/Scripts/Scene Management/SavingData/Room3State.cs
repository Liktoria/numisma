using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room3State : State
{
    public bool mirrorPuzzleSolved = false;
    public bool mirrorClicked = false;
    public bool chestOpened = false;
    public bool cryptexSolutionCorrect = false;
    public bool poetryHintShown = false;
    public bool poetryPuzzleSolved = false;
    public bool morseAlphabetFound = false;
    public bool mirrorFlashing = false;
    public int[] currentCombination = { 0, 0, 0, 0, 0 };
}
