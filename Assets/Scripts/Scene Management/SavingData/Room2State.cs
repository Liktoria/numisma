using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room2State : State
{
    public bool textDiscovered = false;
    public bool insectsSolved = false;
    public bool chestOpened = false;
    public bool usedDisguise = false;
    public bool puzzleComplete = false;
    public int[] currentCombination = { 0, 0, 0, 0, 0 };
    public int currentInsectPage = 0;
}
