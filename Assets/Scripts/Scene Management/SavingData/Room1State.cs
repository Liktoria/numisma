using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room1State : State
{
    public bool initialDialogueDone;
    public bool cardDiscovered;
    public bool talkedToJanitor;
    public bool collectedBook;
    public bool firstCoatContactDone;
    public bool firstDresserContactDone;
    public bool canLeaveRoom;
    public bool firstCameraMovementDone = false;

    public Room1State()
    {
        initialDialogueDone = false;
        cardDiscovered = false;
        talkedToJanitor = false;
        collectedBook = false;
        firstCoatContactDone = false;
        firstDresserContactDone = false;
        canLeaveRoom = false;
    }
}
