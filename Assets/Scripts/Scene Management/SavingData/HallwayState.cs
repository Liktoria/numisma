using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HallwayState : State
{
    public bool treasureDiscovered = false;
    public bool doorRoom2Open = false;
    public bool doorRoom3Open = false;
    public bool firstBellInteraction = true;
    public bool firstBellWithCostume1 = true;
    public bool firstBellWithCostume2 = true;
}
