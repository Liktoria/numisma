using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class State
{
    //Is the character entering the room for the first time?
    public bool firstEntering = true;
    public bool wearingCostume1 = false;
    public bool wearingCostume2 = false;
    public bool textIsShowing = false;
    public bool gameStartedOnce = false;
    public int currentRoom = 1;
}
